<?php

namespace app\commands;

use app\core\entities\Event;
use app\core\entities\Organizer;
use yii\console\Controller;

use Faker\Factory;

class FillController extends Controller
{
    /**
     * @return void
     */
    public function actionInit()
    {
        $faker = Factory::create("ru_RU");

        for ($i = 0; $i <= 5; $i++) {
            $event = Event::make($faker->jobTitle, $faker->date);
            $event->save(false);

            $organizer = Organizer::make($faker->name, $faker->email, $faker->phoneNumber);
            $organizer->save(false);

            $this->stdout("Event \"$event->title\" created.\n");
            $this->stdout("Organizer \"$organizer->fio\" created.\n");
        }

        $this->stdout("Done\n");
    }
}