<?php

namespace app\core\entities;

use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "event".
 *
 * @property int $id
 * @property string $title
 * @property string|null $date
 *
 * @property EventOrganizer[] $eventOrganizers
 * @property Organizer[] $organizers
 */
class Event extends \yii\db\ActiveRecord
{
    /**
     * @param $title
     * @param $date
     * @return static
     */
    public static function make($title, $date): Event
    {
        $entity = new static();
        $entity->title = $title;
        $entity->date = $date;
        return $entity;
    }

    /**
     * @param $title
     * @param $date
     * @return void
     */
    public function edit($title, $date)
    {
        $this->title = $title;
        $this->date = $date;
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'event';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'date' => 'Дата проведения'
        ];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Gets query for [[EventOrganizers]].
     *
     * @return ActiveQuery
     */
    public function getEventOrganizers(): ActiveQuery
    {
        return $this->hasMany(EventOrganizer::class, ['event_id' => 'id']);
    }

    /**
     * Gets query for [[Organizers]].
     *
     * @return ActiveQuery
     * @throws InvalidConfigException
     */
    public function getOrganizers(): ActiveQuery
    {
        return $this->hasMany(Organizer::class, ['id' => 'organizer_id'])->viaTable('event_organizer', ['event_id' => 'id']);
    }
}
