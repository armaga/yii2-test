<?php

namespace app\core\entities;

use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "organizer".
 *
 * @property int $id
 * @property string $fio
 * @property string $email
 * @property string|null $phone
 *
 * @property EventOrganizer[] $eventOrganizers
 * @property Event[] $events
 */
class Organizer extends \yii\db\ActiveRecord
{
    /**
     * @param $fio
     * @param $email
     * @param $phone
     * @return static
     */
    public static function make($fio, $email, $phone): Organizer
    {
        $entity = new static();
        $entity->fio = $fio;
        $entity->email = $email;
        $entity->phone = $phone;
        return $entity;
    }

    /**
     * @param $name
     * @param $email
     * @param $phone
     * @return void
     */
    public function edit($name, $email, $phone)
    {
        $this->fio = $name;
        $this->email = $email;
        $this->phone = $phone;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'organizer';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'fio' => 'ФИО',
            'email' => 'E-mail',
            'phone' => 'Телефон',
        ];
    }

    /**
     * Gets query for [[EventOrganizers]].
     *
     * @return ActiveQuery
     */
    public function getEventOrganizers(): ActiveQuery
    {
        return $this->hasMany(EventOrganizer::class, ['organizer_id' => 'id']);
    }

    /**
     * Gets query for [[Events]].
     *
     * @return ActiveQuery
     * @throws InvalidConfigException
     */
    public function getEvents(): ActiveQuery
    {
        return $this->hasMany(Event::class, ['id' => 'event_id'])->viaTable('event_organizer', ['organizer_id' => 'id']);
    }
}
