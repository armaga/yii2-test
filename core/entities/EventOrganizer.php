<?php

namespace app\core\entities;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "event_organizer".
 *
 * @property int $event_id
 * @property int $organizer_id
 *
 * @property Event $event
 * @property Organizer $organizer
 */
class EventOrganizer extends \yii\db\ActiveRecord
{
    /**
     * @param $eventId
     * @param $organizerId
     * @return static
     */
    public static function make($eventId, $organizerId): EventOrganizer
    {
        $entity = new static();
        $entity->event_id = $eventId;
        $entity->organizer_id = $organizerId;
        return $entity;
    }

    /**
     * @param $eventId
     * @param $organizerId
     * @return void
     */
    public function edit($eventId, $organizerId)
    {
        $this->event_id = $eventId;
        $this->organizer_id = $organizerId;
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'event_organizer';
    }

//    /**
//     * {@inheritdoc}
//     */
//    public function rules()
//    {
//        return [
//            [['event_id', 'organizer_id'], 'required'],
//            [['event_id', 'organizer_id'], 'integer'],
//            [['event_id', 'organizer_id'], 'unique', 'targetAttribute' => ['event_id', 'organizer_id']],
//            [['event_id'], 'exist', 'skipOnError' => true, 'targetClass' => Event::class, 'targetAttribute' => ['event_id' => 'id']],
//            [['organizer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Organizer::class, 'targetAttribute' => ['organizer_id' => 'id']],
//        ];
//    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'event_id' => 'Event ID',
            'organizer_id' => 'Organizer ID',
        ];
    }

    /**
     * Gets query for [[Event]].
     *
     * @return ActiveQuery
     */
    public function getEvent(): ActiveQuery
    {
        return $this->hasOne(Event::class, ['id' => 'event_id']);
    }

    /**
     * Gets query for [[Organizer]].
     *
     * @return ActiveQuery
     */
    public function getOrganizer(): ActiveQuery
    {
        return $this->hasOne(Organizer::class, ['id' => 'organizer_id']);
    }
}
