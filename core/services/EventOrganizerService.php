<?php

namespace app\core\services;

use app\core\entities\EventOrganizer;
use app\core\repositories\EventOrganizerRepository;
use yii\helpers\ArrayHelper;

class EventOrganizerService
{
    private EventOrganizerRepository $eventOrganizers;

    /**
     * @param EventOrganizerRepository $eventOrganizers
     */
    public function __construct(EventOrganizerRepository $eventOrganizers)
    {
        $this->eventOrganizers = $eventOrganizers;
    }

    /**
     * @param $organizerId
     * @param array $events
     * @return void
     */
    public function addEvents($organizerId, array $events)
    {
        if ($events) {
            foreach ($events as $eventId) {
                $entity = EventOrganizer::make($eventId, $organizerId);
                $this->eventOrganizers->save($entity);
            }
        }
    }

    /**
     * @param $organizerId
     * @param array $newEventIds
     * @return void
     */
    public function updateEvents($organizerId, array $newEventIds)
    {
        // Получение текущих ID мероприятий, связанных с организатором
        $currentEventIds = ArrayHelper::getColumn(
            $this->eventOrganizers->findAll(['organizer_id' => $organizerId]), 'event_id'
        );

        // Определение мероприятий для добавления и удаления
        $eventsToAdd = array_diff($newEventIds, $currentEventIds);
        $eventsToRemove = array_diff($currentEventIds, $newEventIds);

        // Удаление устаревших связей
        foreach ($eventsToRemove as $eventId) {
            $this->eventOrganizers->deleteAll(['organizer_id' => $organizerId, 'event_id' => $eventId]);
        }

        // Добавление новых связей
        foreach ($eventsToAdd as $eventId) {
            $entity = EventOrganizer::make($eventId, $organizerId);
            $this->eventOrganizers->save($entity);
        }
    }

    /**
     * @param $eventId
     * @param array $organizers
     * @return void
     */
    public function addOrganizers($eventId, array $organizers)
    {
        if ($organizers) {
            foreach ($organizers as $organizerId) {
                $entity = EventOrganizer::make($eventId, $organizerId);
                $this->eventOrganizers->save($entity);
            }
        }
    }

    /**
     * @param $eventId
     * @param array $newOrganizerIds
     * @return void
     */
    public function updateOrganizers($eventId, array $newOrganizerIds)
    {
        // Получение текущих ID организаторов
        $currentOrganizerIds = ArrayHelper::getColumn(
            $this->eventOrganizers->findAll(['event_id' => $eventId]), 'organizer_id'
        );

        // Определение организаторов для добавления и удаления
        $organizersToAdd = array_diff($newOrganizerIds, $currentOrganizerIds);
        $organizersToRemove = array_diff($currentOrganizerIds, $newOrganizerIds);

        // Удаление устаревших связей
        if (!empty($organizersToRemove)) {
            $this->eventOrganizers->deleteAll(['event_id' => $eventId, 'organizer_id' => $organizersToRemove]);
        }

        // Добавление новых связей
        foreach ($organizersToAdd as $organizerId) {
            $entity = EventOrganizer::make($eventId, $organizerId);
            $this->eventOrganizers->save($entity);
        }
    }

}