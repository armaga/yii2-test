<?php

namespace app\core\services;

use app\core\entities\Event;
use app\core\forms\EventForm;
use app\core\repositories\EventRepository;
use yii\db\StaleObjectException;
use yii\web\NotFoundHttpException;

class EventService
{
    private EventRepository $events;
    private EventOrganizerService $eventOrganizerService;

    /**
     * @param EventRepository $events
     * @param EventOrganizerService $eventOrganizerService
     */
    public function __construct(EventRepository $events, EventOrganizerService $eventOrganizerService)
    {
        $this->events = $events;
        $this->eventOrganizerService = $eventOrganizerService;
    }

    /**
     * @param EventForm $form
     * @return Event
     */
    public function add(EventForm $form): Event
    {
        $entity = Event::make($form->title,$form->date);
        $this->events->save($entity);

        $this->eventOrganizerService->addOrganizers($entity->getId(), $form->associatedOrganizerIds);
        return $entity;
    }

    /**
     * @param $id
     * @param EventForm $form
     * @return Event
     * @throws NotFoundHttpException
     */
    public function edit($id,EventForm $form): Event
    {
        $entity = $this->events->get($id);
        $entity->edit($form->title,$form->date);

        $this->eventOrganizerService->updateOrganizers($entity->getId(), $form->associatedOrganizerIds);

        return $entity;
    }

    /**
     * @param $id
     * @return void
     * @throws \Throwable
     * @throws StaleObjectException
     * @throws NotFoundHttpException
     */
    public function delete($id)
    {
        $entity = $this->events->get($id);
        $this->events->remove($entity);
    }
}