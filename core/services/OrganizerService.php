<?php

namespace app\core\services;


use app\core\entities\Organizer;
use app\core\forms\OrganizerForm;
use app\core\repositories\OrganizerRepository;
use yii\db\StaleObjectException;
use yii\web\NotFoundHttpException;

class OrganizerService
{
    private OrganizerRepository $organizers;
    private EventOrganizerService $eventOrganizerService;

    /**
     * @param OrganizerRepository $organizers
     * @param EventOrganizerService $eventOrganizerService
     */
    public function __construct(OrganizerRepository $organizers, EventOrganizerService $eventOrganizerService)
    {
        $this->organizers = $organizers;
        $this->eventOrganizerService = $eventOrganizerService;
    }

    /**
     * @param OrganizerForm $form
     * @return Organizer
     */
    public function add(OrganizerForm $form): Organizer
    {
        $entity = Organizer::make($form->fio, $form->email, $form->phone);
        $this->organizers->save($entity);

        $this->eventOrganizerService->addEvents($entity->getId(), $form->associatedEventIds);
        return $entity;
    }

    /**
     * @param $id
     * @param OrganizerForm $form
     * @return Organizer
     * @throws NotFoundHttpException
     */
    public function edit($id, OrganizerForm $form): Organizer
    {
        $entity = $this->organizers->get($id);
        $entity->edit($form->fio, $form->email, $form->phone);
        $this->organizers->save($entity);

        $this->eventOrganizerService->updateEvents($entity->getId(), $form->associatedEventIds);

        return $entity;
    }

    /**
     * @param $id
     * @return void
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws StaleObjectException
     */
    public function delete($id)
    {
        $entity = $this->organizers->get($id);
        $this->organizers->remove($entity);
    }
}