<?php

namespace app\core\repositories;

use app\core\entities\Event;
use yii\db\ActiveRecord;
use yii\db\BatchQueryResult;
use yii\db\StaleObjectException;
use yii\web\NotFoundHttpException;

class EventRepository
{
    /**
     * @param $id
     * @return Event
     * @throws NotFoundHttpException
     */
    public function get($id): Event
    {
        if (!$entity = Event::findOne($id)) {
            throw new NotFoundHttpException('Event is not found.');
        }
        return $entity;
    }

    /**
     * @return array|ActiveRecord[]
     */
    public function getAll(): array
    {
        return Event::find()->with('organizers')->all();
    }

    /**
     * @param Event $entity
     * @return void
     */
    public function save(Event $entity)
    {
        if (!$entity->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    /**
     * @param Event $entity
     * @return void
     * @throws \Throwable
     * @throws StaleObjectException
     */
    public function remove(Event $entity)
    {
        if (!$entity->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }
}