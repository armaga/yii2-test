<?php

namespace app\core\repositories;

use app\core\entities\Organizer;
use yii\db\StaleObjectException;
use yii\web\NotFoundHttpException;

class OrganizerRepository
{
    /**
     * @param $id
     * @return Organizer
     * @throws NotFoundHttpException
     */
    public function get($id): Organizer
    {
        if (!$entity = Organizer::findOne($id)) {
            throw new NotFoundHttpException('Organizer is not found.');
        }
        return $entity;
    }

    /**
     * @param Organizer $entity
     * @return void
     */
    public function save(Organizer $entity)
    {
        if (!$entity->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    /**
     * @param Organizer $entity
     * @return void
     * @throws \Throwable
     * @throws StaleObjectException
     */
    public function remove(Organizer $entity)
    {
        if (!$entity->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }
}