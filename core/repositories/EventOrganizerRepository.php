<?php

namespace app\core\repositories;

use app\core\entities\EventOrganizer;
use yii\db\ActiveRecord;
use yii\db\StaleObjectException;
use yii\web\NotFoundHttpException;

class EventOrganizerRepository
{
    /**
     * @param $id
     * @return EventOrganizer
     * @throws NotFoundHttpException
     */
    public function get($id): EventOrganizer
    {
        if (!$entity = EventOrganizer::findOne($id)) {
            throw new NotFoundHttpException('EventOrganizer is not found.');
        }
        return $entity;
    }

    /**
     * @param $condition
     * @return array|ActiveRecord[]
     */
    public function findAll($condition): array
    {
        return EventOrganizer::find()->where($condition)->all();
    }

    /**
     * @param EventOrganizer $entity
     * @return void
     */
    public function save(EventOrganizer $entity)
    {
        if (!$entity->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    /**
     * @param EventOrganizer $entity
     * @return void
     * @throws \Throwable
     * @throws StaleObjectException
     */
    public function remove(EventOrganizer $entity)
    {
        if (!$entity->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }

    /**
     * @param $condition
     * @return void
     */
    public function deleteAll($condition)
    {
        EventOrganizer::deleteAll($condition);
    }
}