<?php

namespace app\core\forms;

use app\core\entities\Event;
use app\core\entities\Organizer;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class OrganizerForm extends Model
{
    /**
     * @var string|null
     */
    public ?string $fio = null;

    /**
     * @var string|null
     */
    public ?string $email = null;

    /**
     * @var string|null
     */
    public ?string $phone = null;

    /**
     * @var array|null
     */
    public ?array $associatedEventIds = null;

    /**
     * @param Organizer|null $organizer
     * @param array $config
     * @throws InvalidConfigException
     */
    public function __construct(?Organizer $organizer = null, array $config = [])
    {
        parent::__construct($config);
        if ($organizer) {
            $this->fio = $organizer->fio;
            $this->email = $organizer->email;
            $this->phone = $organizer->phone;
            $this->associatedEventIds = ArrayHelper::getColumn($organizer->getEvents()->asArray()->all(), 'id');
        }
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['fio', 'email', 'phone'], 'string', 'max' => 255],
            [['fio', 'email', 'associatedEventIds'], 'required'],
            [['associatedEventIds'], 'each', 'rule' => ['in', 'range' => array_keys(self::getEvents())]],
        ];
    }

    /**
     * @return string[]
     */
    public function attributeLabels(): array
    {
        return [
            'fio' => 'ФИО',
            'email' => 'E-mail',
            'phone' => 'Телефон',
            'associatedEventIds' => 'Организаторы',
        ];
    }

    /**
     * @return array
     */
    public static function getEvents(): array
    {
        return ArrayHelper::map(Event::find()->asArray()->all(), 'id', 'title');
    }
}