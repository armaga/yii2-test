<?php

namespace app\core\forms;

use app\core\entities\Event;
use app\core\entities\Organizer;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class EventForm extends Model
{
    /**
     * @var string|null
     */
    public ?string $title = null;

    /**
     * @var string|null Дата в формате 'YYYY-MM-DD'
     */
    public ?string $date = null;


    /**
     * @var array|null
     */
    public ?array $associatedOrganizerIds = null;

    /**
     * @param Event|null $event
     * @param array $config
     * @throws InvalidConfigException
     */
    public function __construct(?Event $event = null, array $config = [])
    {
        parent::__construct($config);
        if ($event) {
            $this->title = $event->title;
            $this->date = $event->date;

            $this->associatedOrganizerIds = ArrayHelper::getColumn($event->getOrganizers()->asArray()->all(), 'id');
        }
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['title', 'date', 'associatedOrganizerIds'], 'required'],
            ['title', 'string', 'max' => 255],
            ['date', 'date', 'format' => 'php:Y-m-d'],
            [['associatedOrganizerIds'], 'each', 'rule' => ['in', 'range' => array_keys(self::getOrganizer())]],
        ];
    }

    /**
     * @return string[]
     */
    public function attributeLabels(): array
    {
        return [
            'title' => 'Название',
            'date' => 'Дата проведения',
            'associatedOrganizerIds' => 'Организаторы'
        ];
    }

    /**
     * @return array
     */
    public static function getOrganizer(): array
    {
        return ArrayHelper::map(Organizer::find()->asArray()->all(), 'id', 'fio');
    }
}