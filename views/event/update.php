<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var \app\core\forms\EventForm $model */
/** @var \app\core\entities\Event $event */

$this->title = 'Обновить мероприятия ' . $event->title;
$this->params['breadcrumbs'][] = ['label' => 'Мероприятии', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $event->title, 'url' => ['view', 'id' => $event->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="event-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
