<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var \app\core\forms\OrganizerForm $model */
/** @var app\core\entities\Organizer $organizer */

$this->title = 'Обновления организатора: ' . $organizer->fio;
$this->params['breadcrumbs'][] = ['label' => 'Организаторы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $organizer->fio, 'url' => ['view', 'id' => $organizer->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="organizer-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
