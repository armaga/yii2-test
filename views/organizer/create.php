<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var \app\core\forms\OrganizerForm $model */

$this->title = 'Создать';
$this->params['breadcrumbs'][] = ['label' => 'Организаторы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="organizer-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
