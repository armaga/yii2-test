<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;


/** @var yii\web\View $this */
/** @var \app\core\forms\OrganizerForm $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="organizer-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::class, [
        'mask' => '+7(999)-999-99-99',
    ]) ?>

    <?= $form->field($model, 'associatedEventIds')->widget(Select2::classname(), [
        'data' => $model::getEvents(),
        'options' => ['placeholder' => 'Выберите организаторов...', 'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
