<?php

/** @var yii\web\View $this */
/** @var \app\core\entities\Event $events */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent mt-5 mb-5">
        <h1 class="display-4">Мероприятия</h1>
    </div>

    <div class="body-content">
        <div class="row">
            <?php /**@var \app\core\entities\Event $event */foreach ($events as $event): ?>
                <div class="col-lg-4 mb-3">
                    <h2><?= htmlspecialchars($event->title) ?></h2>

                    <p>Дата мероприятия: <?= htmlspecialchars($event->date) ?></p>

                    <?php if (!empty($event->organizers)): ?>
                        <p>Организаторы:</p>
                        <ul>
                            <?php foreach ($event->organizers as $organizer): ?>
                                <li><?= htmlspecialchars($organizer->fio) ?></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
