<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%event_organizer}}`.
 */
class m231130_142810_create_event_organizer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('event_organizer', [
            'event_id' => $this->integer()->notNull(),
            'organizer_id' => $this->integer()->notNull(),
            'PRIMARY KEY(event_id, organizer_id)',
        ]);

        // создание внешнего ключа для event_id
        $this->addForeignKey(
            'fk-event_organizer-event_id',
            'event_organizer',
            'event_id',
            'event',
            'id',
            'CASCADE'
        );

        // создание внешнего ключа для organizer_id
        $this->addForeignKey(
            'fk-event_organizer-organizer_id',
            'event_organizer',
            'organizer_id',
            'organizer',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%event_organizer}}');
    }
}
